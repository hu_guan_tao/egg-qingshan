'use strict';

module.exports = app => {
  const DataTypes = app.Sequelize;

  const PayBill = app.model.define('pay_bill', {
    PAY_BILL_ID: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
    },
    USER_ID: {
      type: DataTypes.BIGINT,
      allowNull: true,
    },
    AMOUNT_: {
      type: DataTypes.FLOAT,
      allowNull: true,
    },
    PAY_METHOD: {
      type: DataTypes.BIGINT,
      allowNull: true,
    },
    STATUS_: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
    },
    MOBILE_: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    MERCHANT_ID: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    MERCHANT_NAME: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    MERCHANT_ORDER_ID: {
      type: DataTypes.STRING(100),
      allowNull: true,
    },
    NOTIFY_URL: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    DESCRIPTION_: {
      type: DataTypes.STRING(500),
      allowNull: true,
    },
    NOTICE_PARAM: {
      type: DataTypes.STRING(100),
      allowNull: true,
    },
    SERIAL_NO: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    CREATE_TIME: {
      type: DataTypes.DATE,
      allowNull: true,
    },
    UPDATE_TIME: {
      type: DataTypes.DATE,
      allowNull: true,
    },
  }, {
    // 去除createAt updateAt
    timestamps: false,
    // 使用自定义表名
    freezeTableName: true,
    // 实例对应的表名
    tableName: 'pay_bill',
    // 驼峰匹配
    underscored: false,
  });

  PayBill.associate = function() {

  };

  return PayBill;
};
