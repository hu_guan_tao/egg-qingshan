'use strict';

const Excel = require('exceljs');

async function toSimpleExcel(sheetname, headers, data) {
  const columns = [];// exceljs要求的columns
  const titleRows = headers.length;// 标题栏行数

  // 处理表头
  for (let i = 0; i < titleRows; i++) {
    const row = headers[i];
    if (!row.datatype) {
      row.datatype = '';
    }
    const { key, name, width = 15 } = row;
    if (!key) continue;// 不存在k则跳过
    row.style = { alignment: { vertical: 'middle', horizontal: 'center' } };
    row.header = name;
    row.key = key;
    row.width = width;
    columns.push(row);
  }

  // 生成excel  这一部门语法需看下exceljs
  const workbook = new Excel.Workbook();
  const sheet = workbook.addWorksheet(sheetname || 'sheet1', { views: [{ xSplit: 1, ySplit: 1 }] });
  sheet.columns = columns;
  sheet.addRows(data);
  // 处理样式、日期、字典项
  sheet.eachRow(function(row, rowNumber) {
    // 设置行高
    row.height = 25;
    // 设置边框 黑色 细实线
    const top = { style: 'thin', color: { argb: '000000' } };
    const left = { style: 'thin', color: { argb: '000000' } };
    const bottom = { style: 'thin', color: { argb: '000000' } };
    const right = { style: 'thin', color: { argb: '000000' } };
    row.border = { top, left, bottom, right };
    if (rowNumber <= 1) {
      row.font = { bold: true };
      row.fill = {
        type: 'pattern',
        pattern: 'gray0625',
        // fgColor:{argb:'FFFFFF00'},
        bgColor: { argb: 'CDCDC1' },
      };
      return;
    }
  });

  const writeBuffer = await workbook.xlsx.writeBuffer();
  return writeBuffer;
}
exports.toSimpleExcel = toSimpleExcel;

async function toComplexExcel(sheetname, headers, data) {
  const columns = [];// exceljs要求的columns
  const hjRow = {};// 合计行
  const titleRows = headers.length;// 标题栏行数

  // 处理表头
  for (let i = 0; i < titleRows; i++) {
    const row = headers[i];
    for (let j = 0, rlen = row.length; j < rlen; j++) {
      const col = row[j];
      const { key, name, width = 15 } = col;
      if (!key) continue;// 不存在f则跳过

      if (col.totalRow) hjRow[key] = true;
      if (col.totalRowText) hjRow[key] = col.totalRowText;
      col.style = { alignment: { vertical: 'middle', horizontal: 'center' } };
      col.header = name;
      col.key = key;
      col.width = width;
      columns.push(col);
    }
  }

  // 处理合计行
  if (JSON.stringify(hjRow) !== '{}') {
    const tr = {};
    for (let i = 0, len = data.length; i < len; i++) {
      const item = data[i];
      for (const key in item) {
        if (hjRow[key] === true) {
          tr[key] = (tr[key] || 0) + item[key];
          continue;
        }
        tr[key] = hjRow[key] || '';
      }
    }
    data.push(tr);
  }

  const workbook = new Excel.Workbook();
  const sheet = workbook.addWorksheet(sheetname, { views: [{ xSplit: 1, ySplit: 1 }] });
  sheet.columns = columns;
  sheet.addRows(data);

  // 处理复杂表头
  if (titleRows > 1) {
    for (let i = 1; i < titleRows; i++) sheet.spliceRows(1, 0, []);// 头部插入空行

    for (let i = 0; i < titleRows; i++) {
      const row = headers[i];
      for (let j = 0, rlen = row.length; j < rlen; j++) {
        const col = row[j];
        if (!col.m1) continue;

        sheet.getCell(col.m1).value = col.name;
        sheet.mergeCells(col.m1 + ':' + col.m2);
      }
    }
  }

  // 处理样式、日期、字典项
  const that = this;
  sheet.eachRow(function(row, rowNumber) {
    // 设置行高
    row.height = 25;

    row.eachCell({ includeEmpty: true }, function(cell, colNumber) {
      // 设置边框 黑色 细实线
      const top = { style: 'thin', color: { argb: '000000' } };
      const left = { style: 'thin', color: { argb: '000000' } };
      const bottom = { style: 'thin', color: { argb: '000000' } };
      const right = { style: 'thin', color: { argb: '000000' } };
      cell.border = { top, left, bottom, right };

      // 设置标题部分为粗体
      if (rowNumber <= titleRows) {
        cell.font = { bold: true };
        return;
      }

      // 处理数据项里面的日期和字典
      const { type, dict } = columns[colNumber - 1];
      if (type && (cell.value || cell.value === 0)) {
        return;// 非日期、字典或值为空的直接返回
      }
      switch (type) {
        case 'date':
          cell.value = that.parseDate(cell.value);
          break;
        case 'dict':
          cell.value = that.parseDict(cell.value.toString(), dict);
          break;
        default :
          cell.value = that.parseDate(cell.value);
      }

    });
  });

  const writeBuffer = await workbook.xlsx.writeBuffer();
  return writeBuffer;
}

exports.toComplexExcel = toComplexExcel;
