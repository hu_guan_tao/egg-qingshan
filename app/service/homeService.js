'use strict';

const Service = require('egg').Service;
const svgCaptcha = require('svg-captcha');

class HomeService extends Service {

  async verify() {
    try {
      const captcha = svgCaptcha.create({
        size: 4,
        fontSize: 50,
        width: 100,
        height: 40,
        noise: 1,
        color: true,
        background: '#f2ecde',
      });
      return captcha;
    } catch (err) {
      throw err;
    }
  }

}

module.exports = HomeService;
