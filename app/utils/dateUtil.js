'use strict';

/**
 * Created by hugt on 19/1/12.
 */
const moment = require('moment');
const commonUtil = require('../utils/commonUtil');

function dateUtil() {
}

module.exports = new dateUtil();

/**
 * excel导入格式化时间
 * @param numb 时间,format 格式化规则 'YYYY-MM-DD HH:mm:ss'
 */
dateUtil.prototype.excelFormatDate = numb => {
  if (typeof numb === 'string') {
    numb = commonUtil.trim(numb, 'g');
    return new Date(numb.replace(/-/g, '/'));
  }
  return new Date(1900, 0, numb);

};
/**
 * 格式化时间
 * @param date 时间,rule 格式化规则 'YYYY-MM-DD HH:mm:ss'
 */
dateUtil.prototype.formateDate = (date, rule) => {
  let fDate = '';
  if (typeof date === 'string') {
    fDate = moment(date, 'YYYY-MM-DD HH:mm:ss')
      .format(rule);
  } else {
    fDate = moment(date)
      .format(rule);
  }
  return fDate;
};
/**
 * 计算俩个时间的时间跨度
 * @param date 时间, 折算单位
 */
dateUtil.prototype.timeSpan = (date1, date2, unit, b) => {
  const date1m = moment(date1);
  const date2m = moment(date2);
  return date2m.diff(date1m, unit, b);
};
/**
 * 计算当前时间的未来时间
 * @param date 时间,格式化规则 'YYYY-MM-DD'
 */
dateUtil.prototype.futureDate = (date, count, unit) => {
  return moment(date)
    .add(count, unit)
    .format('YYYY-MM-DD');
};
/**
 * 计算当前时间的未来时间
 * @param date 时间,格式化规则 'YYYY-MM'
 */
dateUtil.prototype.futureMonth = (date, count) => {
  return moment(date)
    .add(count, 'months')
    .format('YYYY-MM');
};
/**
 * 计算当前时间的过去时间
 * @param date 时间,格式化规则 'YYYY-MM-DD'
 */
dateUtil.prototype.formerlyDate = (date, count, unit) => {
  return moment(date)
    .subtract(count, unit)
    .format('YYYY-MM-DD');
};
/**
 * 日期解析，字符串转日期
 * @param dateString 可以为2017-02-16，2017/02/16，2017.02.16
 * @return {Date} 返回对应的日期对象
 */
dateUtil.prototype.dateParse = dateString => {
  const SEPARATOR_BAR = '-';
  const SEPARATOR_SLASH = '/';
  const SEPARATOR_DOT = '.';
  let dateArray;
  if (dateString.indexOf(SEPARATOR_BAR) > -1) {
    dateArray = dateString.split(SEPARATOR_BAR);
  } else if (dateString.indexOf(SEPARATOR_SLASH) > -1) {
    dateArray = dateString.split(SEPARATOR_SLASH);
  } else {
    dateArray = dateString.split(SEPARATOR_DOT);
  }
  return new Date(dateArray[0], dateArray[1] - 1, dateArray[2]);
};

/**
 * 日期比较大小
 * compareDateString大于dateString，返回1；
 * 等于返回0；
 * compareDateString小于dateString，返回-1
 * @param dateString 日期
 * @param compareDateString 比较的日期
 */
dateUtil.prototype.dateCompare = (dateString, compareDateString) => {
  const dateTime = dateUtil.prototype.dateParse(dateString)
    .getTime();
  const compareDateTime = dateUtil.prototype.dateParse(compareDateString)
    .getTime();
  if (compareDateTime > dateTime) {
    return 1;
  } else if (compareDateTime === dateTime) {
    return 0;
  }
  return -1;

};

/**
 * 判断日期是否在区间内，在区间内返回true，否返回false
 * @param dateString 日期字符串
 * @param startDateString 区间开始日期字符串
 * @param endDateString 区间结束日期字符串
 * @return {Number}
 */
dateUtil.prototype.isDateBetween = (dateString, startDateString, endDateString) => {
  let flag = false;
  const startFlag = (dateUtil.prototype.dateCompare(dateString, startDateString) < 1);
  const endFlag = (dateUtil.prototype.dateCompare(dateString, endDateString) > -1);
  if (startFlag && endFlag) {
    flag = true;
  }
  return flag;
};
/**
 * 判断日期区间[startDateCompareString,endDateCompareString]是否完全在别的日期区间内[startDateString,endDateString]
 * 即[startDateString,endDateString]区间是否完全包含了[startDateCompareString,endDateCompareString]区间
 * 在区间内返回true，否返回false
 * @param startDateString 新选择的开始日期，如输入框的开始日期
 * @param endDateString 新选择的结束日期，如输入框的结束日期
 * @param startDateCompareString 比较的开始日期
 * @param endDateCompareString 比较的结束日期
 * @return {Boolean}
 */
dateUtil.prototype.isDatesBetween = (startDateString, endDateString, startDateCompareString, endDateCompareString) => {
  let flag = false;
  const startFlag = (dateUtil.prototype.dateCompare(startDateCompareString, startDateString) < 1);
  const endFlag = (dateUtil.prototype.dateCompare(endDateCompareString, endDateString) > -1);
  if (startFlag && endFlag) {
    flag = true;
  }
  return flag;
};

/**
 * 获取指定月第一天
 * @param dateString 日期字符串
 * @return {string}
 */
dateUtil.prototype.monthOfFirstDay = dateString => {
  return moment(dateString)
    .startOf('month')
    .format('YYYY-MM-DD');
};
/**
 * 获取指定月最后一天
 * @param dateString 日期字符串
 * @return {string}
 */
dateUtil.prototype.monthOfLastDay = dateString => {
  return moment(dateString)
    .endOf('month')
    .format('YYYY-MM-DD');
};
/**
 * 获取指定月天数
 * @param dateString 日期字符串
 * @return {Number}
 */
dateUtil.prototype.monthOfDays = dateString => {
  return moment(dateString)
    .daysInMonth();
};

/**
 * 获取年、月、日
 * @param dateString 日期字符串
 * @param unit 年、月、日
 * @return {Number}
 */
dateUtil.prototype.ymr = (dateString, unit) => {
  return moment(dateString)
    .get(unit);
};
/**
 * 获取俩个时间段之间包含一个日期的次数
 * @param startDate 日期字符串
 * @param endDate 日期字符串
 * @param dateString 月日
 * @return {Number}
 */
dateUtil.prototype.isDateBetweenCount = (startDate, endDate, dateString) => {
  const intervalDays = dateUtil.prototype.timeSpan(startDate, endDate, 'days', false) + 1;
  let i = 0;
  const arr = [];
  for (let j = 0; j < intervalDays; j++) {
    const obj = {
      date: '',
    };
    obj.date = dateUtil.prototype.futureDate(startDate, j, 'days');
    const md = obj.date.substring(5, obj.date.length);
    if (md == dateString) {
      arr.push(obj);
      i = i + 1;
    }
  }
  return i;
};
/**
 * 判断一个数组中多个时间段是否有重叠的情况
 * @param Array 数组
 * @return {Boolean}
 */
dateUtil.prototype.isDateOverlapArray = dateArr => {
  const newArr = [];
  let index = 0;
  for (let i = 0; i < dateArr.length; i++) {
    const dateInfo = dateArr[i];
    for (let j = 0; j < newArr.length; j++) {
      const newInfo = newArr[j];
      if (dateUtil.prototype.isDateBetween(dateInfo.effectiveStartDate, newInfo.effectiveStartDate, newInfo.effectiveEndDate) || dateUtil.prototype.isDateBetween(dateInfo.effectiveEndDate, newInfo.effectiveStartDate, newInfo.effectiveEndDate)) {
        index++;
      }
    }
    newArr.push(dateInfo);
  }
  if (index > 0) {
    return true;
  }
  return false;

};
