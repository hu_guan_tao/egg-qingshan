'use strict';

module.exports = {
  // 让egg框架统一处理异常的方法, 需要配置
  handleError: (err, ctx) => {
    if (err.ext) {
      // 开发知晓的错误流程, 抛出异常并附带信息用于展示
      ctx.status = 200;
      ctx.set('Content-Type', 'application/json');
      const body = {
        code: err.ext.code,
        message: ctx.__(err.ext.msg, ...err.ext.params),
        content: {},
      };
      if (ctx.request.accepts('text', 'json') === 'json') {
        ctx.body = body;
      } else {
        ctx.body = JSON.stringify(body);
      }
    } else {
      ctx.status = 500;
      ctx.set('Content-Type', 'application/json');
      const body = {
        code: 'error',
        message: ctx.__('DEFAULTERROR'),
        content: {},
      };
      if (ctx.request.accepts('text', 'json') === 'json') {
        ctx.body = body;
      } else {
        ctx.body = JSON.stringify(body);
      }
    }
  },
  // 开发知晓的错误, 抛出异常用于中断流程, 会在handleError中接收并处理
  // 若msg所对应的国际化文本包含占位符, 则需要传params去替代占位符
  // 如: zh-CN中 'TEXT': '测试%s', 若msg传TEXT, params传 ['aaa'], 则返回'测试aaa'
  throwError: (code, msg, ...params) => {
    const error = new Error(msg);
    error.ext = { code, msg, params };
    throw error;
  },
};
