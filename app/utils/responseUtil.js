'use strict';

const errorUtil = require('../utils/errorUtil');
module.exports = {
  error: (ctx, statusCode, data, e) => {
    ctx.response.status = statusCode;
    ctx.response.body = {
      code: 'error',
      message: ctx.__(e),
      content: data,
    };
  },
  success: (ctx, msg, data) => {
    ctx.response.status = 200;
    ctx.response.body = {
      code: 'success',
      message: msg ? ctx.__(msg) : 'success',
      content: data,
    };
  },
  validate: (msg, ...params) => {
    errorUtil.throwError('error', msg, ...params);
  },
};
