'use strict';

/**
 * Created by hugt on 17/8/16.
 */
const underscore = require('underscore');

function commonUtil() {
}

module.exports = new commonUtil();

/**
 * 去除字符串中的所有空格
 * @param str 字符串,is_global true or false 是否全部去除空格'
 */
commonUtil.prototype.trim = (str, is_global) => {
  let result;
  if (is_global && is_global === 'g') {
    result = str.replace(/\s/g, '');
  } else {
    result = str.replace(/(^\s*)|(\s*$)/g, '');
  }
  return result;
};
/**
 * 数组转小写
 * @param array 数组'
 */
commonUtil.prototype.toLowerArray = array => {
  const ar = [];
  if (array) {
    for (let i = 0; i < array.length; i++) {
      ar.push(commonUtil.prototype.toLower(array[i]));
    }
  }
  return ar;
};
/**
 * 数组转大写
 * @param array 数组'
 */
commonUtil.prototype.toUpperArray = array => {
  const ar = [];
  if (array) {
    for (let i = 0; i < array.length; i++) {
      ar.push(commonUtil.prototype.toUpper(array[i]));
    }
  }
  return ar;
};
/**
 * 对象转小写
 * @param obj 对象'
 */
commonUtil.prototype.toLower = obj => {
  const newobj = {};
  for (const key in obj) {
    newobj[key.toLowerCase()] = obj[key];
  }
  return newobj;
};
/**
 * 对象转大写
 * @param obj 对象'
 */
commonUtil.prototype.toUpper = obj => {
  const newobj = {};
  for (const key in obj) {
    newobj[key.toUpperCase()] = obj[key];
  }
  return newobj;
};
/**
 * 对象字段值为空转0
 * @param obj 对象'
 */
commonUtil.prototype.objAttrIsNullToZero = obj => {
  for (const i in obj) {
    if (!obj[i]) {
      obj[i] = 0;
    }
  }
  return obj;
};
/**
 * 对象字段值为undefined转""
 * @param obj 对象'
 */
commonUtil.prototype.objAttrIsUndefinedToEmptyString = obj => {
  for (const i in obj) {
    if (obj[i] === 'undefined') {
      obj[i] = '';
    }
  }
  return obj;
};
/**
 * 对象字段值为空转""
 * @param obj 对象'
 */
commonUtil.prototype.objAttrIsNullToEmptyString = obj => {
  for (const i in obj) {
    if (!obj[i] && obj[i] !== 0) {
      obj[i] = '';
    }
  }
  return obj;
};
/**
 * 对象特殊字符转义
 * @param obj 对象'
 */
commonUtil.prototype.objSpecialCharacterEscape = obj => {
  for (const i in obj) {
    if (obj[i]) {
      if (typeof obj[i] === 'string') {
        obj[i] = obj[i].replace(/&/g, '&amp;');
        obj[i] = obj[i].replace(/</g, '&lt;');
        obj[i] = obj[i].replace(/>/g, '&gt;');
      }
    }
  }
  return obj;
};
/**
 * 字符串特殊字符转义
 * @param str 字符串'
 */
commonUtil.prototype.stringSpecialCharacterEscape = str => {
  str = str.replace(/&/g, '&amp;');
  str = str.replace(/</g, '&lt;');
  str = str.replace(/>/g, '&gt;');
  return str;
};
/**
 * 字符串补零
 * @param num 字符串总长度,str 需要补零的字符串'
 */
commonUtil.prototype.zeroCompensation = (num, str) => {
  const lg = str.length;
  const count = num - lg;
  if (count > 0) {
    for (let i = 0; i < count; i++) {
      str = '0' + str;
    }
  }
  return str;
};
/**
 * 克隆数组
 * @param array 数组'
 */
commonUtil.prototype.cloneArray = array => {
  if (!commonUtil.prototype.isArray(array)) {
    return array;
  }
  const r = [];
  for (const i in array) {
    r.push(underscore.clone(array[i]));
  }
  return r;

};
/**
 * 是否是数组
 * @param v 数组'
 */
commonUtil.prototype.isArray = v => {
  return toString.apply(v) === '[object Array]';
};
/**
 * 判断数据中是否存在指定字符串
 * @param array 数组,str 指定字符串'
 */
commonUtil.prototype.arrContains = (array, str) => {
  if (array.indexOf(str) >= 0) {
    return true;
  }
  return false;

};
/**
 * 根据指定值删除数组中匹配的元素
 * @param array 数组,value 指定元素'
 */
commonUtil.prototype.filerArrayByValue = (array, value) => {
  let index = 0;
  if (typeof value === 'object') {
    index = commonUtil.prototype.findIndex(array, value);
  } else {
    index = array.indexOf(value);
  }
  if (index >= 0) {
    return array.splice(index, 1);
  }
  return array;

};
/**
 * 根据指定值查询在数组中的下标
 * @param array 数组,obj 指定元素'
 */
commonUtil.prototype.findIndex = (arr, obj) => {
  const objStr = JSON.stringify(obj);
  return arr.reduce((index, ele, i) => {
    if (JSON.stringify(ele) === objStr) {
      return i;
    }
    return index;

  }, -1);
};
/**
 * 字符串部分隐藏处理
 * @param str 字符串,frontLen 保留的前几位,endLen 保留的后几位,cha 替换的字符串'
 */
commonUtil.prototype.strHide = (str, frontLen, endLen, cha) => {
  const len = str.length - frontLen - endLen;
  let xing = '';
  for (let i = 0; i < len; i++) {
    xing += cha;
  }
  return str.substring(0, frontLen) + xing + str.substring(str.length - endLen);
};
/**
 * 数组去重
 * @param arr 数组'
 */
commonUtil.prototype.arrayQC = arr => {
  const narr = [];
  for (let i = 0; i < arr.length; i++) {
    let notIn = true;
    for (let j = 0; j < narr.length; j++) {
      const n1 = arr[i];
      const n2 = narr[j];
      if (underscore.isEqual(n1, n2)) {
        notIn = false;
        break;
      }
    }
    if (notIn) {
      narr.push(arr[i]);
    }
  }
  return narr;
};
/**
 * 添加到数组
 * @param mergeTo 数组,mergeFrom 数组'
 */
commonUtil.prototype.arrayAdd = (mergeTo, mergeFrom) => {
  Array.prototype.push.apply(mergeTo, mergeFrom);
  return mergeTo;
};
/**
 * 填充单引号
 * @param str 字符串'
 */
commonUtil.prototype.getDyString = str => {
  const strs = str.split(',');
  let string = '';
  for (const i in strs) {
    string += '\'' + strs[i] + '\',';
  }
  string = string.substring(0, string.length - 1);
  return string;
};
/**
 * 判断空对象
 * @param obj 对象'
 */
commonUtil.prototype.isEmptyObject = obj => {
  for (const key in obj) {
    return false;
  }
  return true;
};
/**
 * 判断空对象
 * @param obj 对象'
 */
commonUtil.prototype.isNotEmptyObject = obj => {
  for (const key in obj) {
    return true;
  }
  return false;
};
/**
 * 查找字典表数组中编码对应的描述
 * @param code 编码,arr 数组'
 */
commonUtil.prototype.codeToDesc = (code, arr) => {
  let desc = '';
  for (let i = 0; i < arr.length; i++) {
    if (code == arr[i].CODE) {
      desc = arr[i].DESCRIPTION;
    }
  }
  return desc;
};
/**
 * 字节大小自动转整数
 * @param bytes 字节大小'
 */
commonUtil.prototype.byteConvert = bytes => {
  if (isNaN(bytes)) {
    return '';
  }
  const symbols = [ 'bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB' ];
  let exp = Math.floor(Math.log(bytes) / Math.log(2));
  if (exp < 1) {
    exp = 0;
  }
  const i = Math.floor(exp / 10);
  bytes = bytes / Math.pow(2, 10 * i);

  if (bytes.toString().length > bytes.toFixed(2)
    .toString().length) {
    bytes = bytes.toFixed(2);
  }
  return bytes + ' ' + symbols[i];
};
/**
 * 判断字符串是不是json字符串
 * @param str 字符串'
 */
commonUtil.prototype.strIsJson = str => {
  if (typeof str === 'string') {
    try {
      const obj = JSON.parse(str);
      if (typeof obj === 'object' && obj) {
        return true;
      }
      return false;

    } catch (e) {
      return false;
    }
  }
};
/**
 * 判断数组是否包含某个字符串
 * @param arr 字符串数组,str 字符串'
 */
commonUtil.prototype.arrContainStr = function(arr, str) {
  let index = arr.length;
  while (index--) {
    if (arr[index] === str) {
      return true;
    }
  }
  return false;
};
/**
 * 数组合并并去重
 * @param arr1 数组1,arr2 数组2'
 */
commonUtil.prototype.arrMergeQc = function(arr1, arr2) {
  let arr = [];
  for (const arrInfo1 of arr1) {
    arr.push(arrInfo1);
    for (const arrInfo2 of arr2) {
      arr.push(arrInfo2);
    }
  }
  arr = commonUtil.prototype.arrayQC(arr);
  return arr;
};
/**
 * 是否是数值类型字符串
 * @param str 字符串'
 */
commonUtil.prototype.isNumber = function(str) {
  const n = Number(str);
  if (!isNaN(n)) {
    return true;
  }
  return false;

};
/**
 * 数值格式化
 * @param num 数值'
 */
commonUtil.prototype.formatNumber = num => {
  if (typeof num === 'string') {
    num = commonUtil.prototype.trim(num, 'g');
    if (commonUtil.prototype.isNumber(num)) {
      return parseFloat(num);
    }
    return 0;

  }
  return num;

};
/**
 * 计算两位数应该乘以的数，为加减乘数服务的函数
 * @param num1 数值1,num2 数值2'
 */
commonUtil.prototype.formatNumber2 = (num1, num2) => {
  let r1,
    r2;
  try {
    r1 = num1.toString()
      .split('.')[1].length;
  } catch (e) {
    r1 = 0;
  }
  try {
    r2 = num2.toString()
      .split('.')[1].length;
  } catch (e) {
    r2 = 0;
  }
  const sum = r1 + r2;
  const sub = r2 - r1;
  return {
    max: Math.pow(10, Math.max(r1, r2)),
    sum: Math.pow(10, sum),
    sub: Math.pow(10, sub),
  };
};
/**
 * 加法
 * @param num1 数值1,num2 数值2,n 保留位数'
 */
commonUtil.prototype.plus = (num1, num2, n) => {
  const formatNum = commonUtil.prototype.formatNumber2(num1, num2).max * 100;
  const result = (num1 * formatNum + num2 * formatNum) / formatNum;
  if (n) {
    return result.toFixed(n);
  }
  return result;
};
/**
 * 减法
 * @param num1 数值1,num2 数值2,n 保留位数'
 */
commonUtil.prototype.sub = (num1, num2, n) => {
  const formatNum = commonUtil.prototype.formatNumber2(num1, num2).max * 100;
  const result = (num1 * formatNum - num2 * formatNum) / formatNum;
  if (n) {
    return result.toFixed(n);
  }
  return result;
};
// 俩个数组的交集
commonUtil.prototype.intersect = (arr1, arr2) => {
  const num1 = [ ...new Set(arr1) ];
  const num2 = [ ...new Set(arr2) ];
  const result = [];
  if (num1.length <= num2.length) {
    for (let i = 0; i < num1.length; i++) {
      for (let j = 0; j < num2.length; j++) {
        if (num1[i] === num2[j]) {
          result.push(num1[i]);
        }
      }
    }
  } else {
    for (let i = 0; i < num2.length; i++) {
      for (let j = 0; j < num1.length; j++) {
        if (num2[i] === num1[j]) {
          result.push(num2[i]);
        }
      }
    }
  }

  return result;
};
// 俩个数组差集
commonUtil.prototype.subArr = (arr1, arr2) => {
  const set1 = new Set(arr1);
  const set2 = new Set(arr2);
  const subset = [];
  for (const item of set1) {
    if (!set2.has(item)) {
      subset.push(item);
    }
  }

  return subset;
};
// 保留n位小数
commonUtil.prototype.toDecimal = (num, n) => {
  if (num) {
    if (typeof num === 'string') {
      num = commonUtil.prototype.trim(num, 'g');
      if (commonUtil.prototype.isNumber(num)) {
        return parseFloat(parseFloat(num).toFixed(n));
      }
      return 0;

    }
    return parseFloat(num.toFixed(n));

  }
  return 0;

};
/**
 * 时间格式化
 * @param date 数值
 */
commonUtil.prototype.formatDate = date => {
  if (date) {
    if (typeof date === 'string') {
      return new Date(date);
    }
    return date;

  }
  return '';

};
/**
 * 可能json对象
 * @param obj json对象
 */
commonUtil.prototype.cloneJsonObj = function(obj) {
  return JSON.parse(JSON.stringify(obj));
};

// eslint-disable-next-line no-extend-native
Number.prototype.toFixed = function(d) {
  let s = this + '';
  if (!d) d = 0;
  if (s.indexOf('.') === -1) s += '.';
  s += new Array(d + 1).join('0');
  if (new RegExp('^(-|\\+)?(\\d+(\\.\\d{0,' + (d + 1) + '})?)\\d*$').test(s)) {
    let ss = '0' + RegExp.$2;
    const pm = RegExp.$1;
    let a = RegExp.$3.length;
    let b = true;
    if (a === d + 2) {
      a = ss.match(/\d/g);
      if (parseInt(a[a.length - 1]) > 4) {
        for (let i = a.length - 2; i >= 0; i--) {
          a[i] = parseInt(a[i]) + 1;
          if (a[i] === 10) {
            a[i] = 0;
            b = i !== 1;
          } else break;
        }
      }
      ss = a.join('').replace(new RegExp('(\\d+)(\\d{' + d + '})\\d$'), '$1.$2');
    }
    if (b) ss = ss.substr(1);
    return (pm + ss).replace(/\.$/, '');
  }
  return this + '';
};
