'use strict';

/**
 * Created by hugt on 2019/9/7.
 */
const fs = require('fs');

function fileUtil() {
}

module.exports = new fileUtil();

/**
 * 读取文件
 * @param numb 时间,format 格式化规则 'YYYY-MM-DD HH:mm:ss'
 */
fileUtil.prototype.readFile = path => {
  return fs.readFileSync(path, 'utf8');
};
