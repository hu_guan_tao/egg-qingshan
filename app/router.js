'use strict';

/**
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  require('./router/homeRouter')(app);
  require('./router/uploadRouter')(app);// 上传文件
};
