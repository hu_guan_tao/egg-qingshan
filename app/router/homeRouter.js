'use strict';

module.exports = app => {
  const { router, controller, jwt } = app;
  const { homeController } = controller;
  router.post('/login', homeController.login);
  router.get('/', homeController.index);
  router.get('/verify', homeController.verify); // 验证码
  router.get('/simpleexcel', jwt, homeController.simpleexcel);
  router.get('/complexExcel', jwt, homeController.complexExcel);
  router.get('/payBillList', homeController.payBillList);
};
