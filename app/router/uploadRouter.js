'use strict';

module.exports = app => {
  const { router, controller } = app;
  const { uploadController } = controller;
  router.post('/prada/upload', uploadController.upload);
};
