'use strict';

const Controller = require('egg').Controller;
const res = require('../utils/responseUtil');
/**
 * @Controller Home
 */
class HomeController extends Controller {

  /**
   * @summary 获取token
   * @Description
   * @Router POST /login
   * @Response 200
   */
  async login() {
    const { ctx, app } = this;
    const data = ctx.request.body;
    const token = app.jwt.sign({ username: data.username }, app.config.jwt.secret);
    ctx.body = token;
  }

  async index() {
    const { ctx } = this;
    ctx.body = ctx.utils.fileUtil.readFile('././banner.txt');
  }

  /**
   * @summary 获取验证码
   * @Description
   * @Router GET /verify
   * @Response 200
   */
  async verify() {
    const { ctx, service } = this;
    const captcha = await service.homeService.verify();
    ctx.response.type = 'image/svg+xml';
    ctx.body = captcha.data;
  }

  async simpleexcel() {
    const { ctx } = this;
    const header = [
      { key: 'EMPUID', name: '工号' },
      { key: 'CNNAME', name: '姓名' },
      { key: 'SEX', name: '性别', width: 10 },
      { key: 'DATE', name: '生日' },
    ];
    const data = [{
      EMPUID: 123,
      CNNAME: '胡官韬',
      SEX: '男',
      DATE: new Date(),
    }, {
      EMPUID: 123,
      CNNAME: '胡官韬',
      SEX: '男',
      DATE: null,
    }];
    const buffer = await ctx.helper.toSimpleExcel('', header, data);
    ctx.attachment('角色人员列表导出.xlsx');
    ctx.body = buffer;
  }

  async complexExcel() {
    const { ctx } = this;
    const header = [[
      { name: '单位名称', key: 'deptName', w: 20, m1: 'A1', m2: 'A3', totalRowText: '合计' },
      { name: '办理身份证证件', m1: 'B1', m2: 'M1' },
      { name: '临时身份证证件', m1: 'N1', m2: 'O1' },
      { name: '总计', m1: 'P1', m2: 'R2' },
    ], [
      { name: '申领', m1: 'B2', m2: 'D2' },
      { name: '换领', m1: 'E2', m2: 'G2' },
      { name: '补领', m1: 'H2', m2: 'J2' },
      { name: '小计', m1: 'K2', m2: 'M2' },
      { name: '临时身份证', m1: 'N2', m2: 'O2' },
    ], [
      { name: '本地人数', key: 'slbdCount', totalRow: true },
      { name: '异地人数', key: 'slydCount', totalRow: true },
      { name: '金额', key: 'slJe', totalRow: true },
      { name: '本地人数', key: 'hlbdCount', totalRow: true },
      { name: '异地人数', key: 'hlydCount', totalRow: true },
      { name: '金额', key: 'hlJe', totalRow: true },
      { name: '本地人数', key: 'blbdCount', totalRow: true },
      { name: '异地人数', key: 'blydCount', totalRow: true },
      { name: '金额', key: 'blJe', totalRow: true },
      { name: '本地人数', key: 'xj_bdrs', totalRow: true },
      { name: '异地人数', key: 'xj_ydrs', totalRow: true },
      { name: '金额', key: 'xj_je', totalRow: true },
      { name: '人数', key: 'lsCount', totalRow: true },
      { name: '金额', key: 'lsJe', totalRow: true },
      { name: '本地人数', key: 'hj_bdrs', totalRow: true },
      { name: '异地人数', key: 'hj_ydrs', totalRow: true },
      { name: '金额', key: 'hj_je', totalRow: true },
    ]];
    const data = [];
    const buffer = await ctx.helper.toComplexExcel('', header, data);
    ctx.attachment('角色人员列表导出.xlsx');
    ctx.body = buffer;
  }

  /**
   * @summary 付款单列表
   * @Description
   * @Router GET /payBillList
   * @Request query integer limit 每页显示个数
   * @Request query integer offset 页码
   * @Response 200
   */
  async payBillList() {
    const { ctx } = this;
    const query = { limit: parseInt(ctx.query.limit), offset: parseInt(ctx.query.offset) };
    const payBillList = await ctx.model.PayBill.findAll(query);
    res.success(ctx, 200, payBillList);
  }
}

module.exports = HomeController;
