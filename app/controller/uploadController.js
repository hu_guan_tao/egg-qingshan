'use strict';

const Controller = require('egg').Controller;
const fs = require('fs');
const p = require('../../properties');
const path = require('path');
const pump = require('pump');
const res = require('../utils/responseUtil');

class UploadController extends Controller {
  async upload() {
    const { ctx } = this;
    for (const file of ctx.request.files) {
      const date = ctx.utils.dateUtil.formateDate(new Date(), 'YYYYMMDD');
      const filePath = p.fileDir + date;
      if (!fs.existsSync(filePath)) {
        fs.mkdirSync(filePath);
      }
      const filenameArr = file.filename.split('.');
      const fileName = Date.now() + '.' + filenameArr[filenameArr.length - 1];
      const targetPath = path.join(filePath, fileName);
      const fileSize = fs.readFileSync(file.filepath).length;
      if (fileSize > 104857600) {
        res.validate('file_size_exceeded');
      }
      const fileSizeDesc = ctx.utils.commonUtil.byteConvert(fileSize);
      const source = fs.createReadStream(file.filepath);
      const target = fs.createWriteStream(targetPath);
      await pump(source, target);
      const data = {
        url: p.fileDir + date + '/' + fileName,
        path: filePath + '/' + fileName,
        relativePath: +'/' + date + '/' + fileName,
        oldName: file.filename,
        newName: fileName,
        size: fileSizeDesc,
        fileSize,
        fileSizeDesc,
        userId: 'sys',
      };
      res.success(ctx, 200, data);
    }
  }

}

module.exports = UploadController;
