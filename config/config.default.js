/* eslint valid-jsdoc: "off" */
'use strict';
const path = require('path');
const p = require(process.cwd() + '/properties');
/**
 * @param {Egg.EggAppInfo} appInfo app info
 */
module.exports = appInfo => {
  /**
   * built-in config
   * @type {Egg.EggAppConfig}
   **/
  const config = exports = {};
  // debug为 true时,用于本地调试
  config.debug = true;
  // use for cookie sign key, should change to your own and keep security
  config.keys = appInfo.name + '_hugt';
  config.cluster = {
    listen: {
      path: '',
      port: p.port,
      hostname: 'localhost',
    },
  };
  // 业务日志
  config.logger = {
    outputJSON: false,
    dir: p.serviceLogDir,
  };
  // 定时日志
  config.customLogger = {
    scheduleLogger: {
      consoleLevel: 'NONE',
      file: path.join(p.scheduleLogDir, 'egg-schedule.log'),
    },
  };
  // 中间件
  config.middleware = p.middleware;
  // sequelize多数据源配置
  config.sequelize = {
    datasources: [
      {
        delegate: 'model', // 指定加载实体类时使用app.model and ctx.model
        baseDir: 'model', // 指定实体类存放的目录
        username: p.db.qingshan.user,
        password: p.db.qingshan.password,
        dialect: 'mysql',
        host: p.db.qingshan.host,
        port: p.db.qingshan.port,
        database: p.db.qingshan.database,
      }],
  };
  // redis相关配置
  config.redis = {
    client: {
      host: p.redis.host,
      port: p.redis.port,
      password: p.redis.password,
      db: '0',
    },
  };
  config.multipart = {
    mode: 'file',
    fileSize: '1000mb',
    whitelist: [
      '.txt',
      '.apk',
      '.xlsx',
      '.xls',
      '.doc',
      '.docx',
      '.pptx',
      '.ppt',
      '.pdf',
      '.csv',
      '.msg',
      '.jpg',
      '.jpeg',
      '.png',
      '.gif',
      '.bmp',
      '.wbmp',
      '.webp',
      '.tif',
      '.psd',
      '.svg',
      '.rar',
      '.zip',
      '.gz',
      '.tgz',
      '.gzip',
    ],
  };
  // redis锁
  config.locker = {
    name: '',
    retryLater: 20, // 没有获取到锁的时候，过多长时间再次尝试获取锁，单位: ms
    timeout: 60000, // 超过多长时间没有获取到锁，则获取锁失败, 单位: ms
    ttl: 10, // 锁的超时时间, 单位: s
  };
  // 相对app目录的路径，默认值utils
  config.utils = {
    path: 'utils',
  };
  /**
   * logview default config
   * @member Config#logview
   * @property {String} prefix - logview route prefix, default to `__logs`
   * @property {String} dir - logview dir, default to `app.config.logger.dir`
   */
  config.logview = {
    prefix: 'logs',
    dir: p.serviceLogDir,
  };
  config.passportLocal = {
    // usernameField: 'username',
    // passwordField: 'password',
  };
  /**
   *前端请求需要在header头上加
   * 切记 token 不要直接发送，要在前面加上 Bearer 字符串和一个空格
   *'Authorization':`Bearer ${token}`
   */
  config.jwt = {
    secret: '123456',
  };
  config.static = {
    prefix: '/',
    dir: process.cwd() + '/public',
  };
  config.rundir = process.cwd() + '/run';
  config.swaggerdoc = {
    dirScanner: './app/controller',
    // basePath: '/',
    apiInfo: {
      title: 'egg-qingshan',
      description: '众生皆草木，唯你是青山。',
      version: '1.0.0',
    },
    schemes: [ 'http', 'https' ],
    consumes: [ 'application/json' ],
    produces: [ 'application/json' ],
    securityDefinitions: {
      // apikey: {
      //   type: 'apiKey',
      //   name: 'clientkey',
      //   in: 'header',
      // },
      // oauth2: {
      //   type: 'oauth2',
      //   tokenUrl: 'http://petstore.swagger.io/oauth/dialog',
      //   flow: 'password',
      //   scopes: {
      //     'write:access_token': 'write access_token',
      //     'read:access_token': 'read access_token',
      //   },
      // },
    },
    enableSecurity: false,
    // enableValidate: true,
    routerMap: false,
    enable: true,
  };
  // add your user config here
  const userConfig = {
    // myAppName: 'egg',
  };

  return {
    ...config,
    ...userConfig,
  };
};
