'use strict';

const sequelize = {
  enable: true,
  package: 'egg-sequelize',
};
const redis = {
  enable: false,
  package: 'egg-redis',
};
const locker = {
  enable: true,
  package: 'egg-redlock-plugin',
};
const utils = {
  enable: true,
  package: 'egg-easy-utils',
};
const security = {
  enable: false,
};
const logview = {
  enable: true,
  package: 'egg-logview',
  // env: ['local', 'default', 'test', 'unittest']
};
const passport = {
  enable: false,
  package: 'egg-passport',
};
const passportLocal = {
  enable: false,
  package: 'egg-passport-local',
};
const jwt = {
  enable: true,
  package: 'egg-jwt',
};
const swaggerdoc = {
  enable: true,
  package: 'egg-swagger-doc',
};
exports.sequelize = sequelize;
exports.redis = redis;
exports.locker = locker;
exports.utils = utils;
exports.security = security;
exports.logview = logview;
exports.passport = passport;
exports.passportLocal = passportLocal;
exports.jwt = jwt;
exports.swaggerdoc = swaggerdoc;
